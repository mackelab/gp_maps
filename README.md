# GPmaps: Estimating Orientation Preference Maps using Gaussian Process Methods #

 Original website with code on the bethgelab pages: [http://bethgelab.org/code/macke2010/](http://bethgelab.org/code/macke2010/)

See [https://bitbucket.org/mackelab/home](https://bitbucket.org/mackelab/home) for more repositories by Jakob Macke and colleagues.


## Description ##

MATLAB code implementing the Gaussian Process based methods for estimating orientation preference maps from optical imaging data as described in Macke et al, 2009 and Macke at al, 2010. The package contains code for:

* Generating synthetic orientation preference maps and noisy imaging measurements

* Estimating orientation preference maps using Gaussian process methods or vector averaging

* Sampling from the posterior distribution over maps

While the current implementation is useful for orientation preference maps only, it should be straightforward to adapt it for the estimation of other cortical maps as well.

If you notice a bug, want to request a feature, or have a question or feedback, please make use of the issue-tracking capabilities of the repository. We love to hear from people using our code-- please send an email to info@mackelab.org.

## Installation ##

Either check out the repository, or download the (initial commit) as a [.zip-file](https://bitbucket.org/mackelab/gp_maps/downloads/cleancode.zip).
Add all the folders originating from it to your MATLAB path. Then, run the script DemoScript.m, which contains a brief tutorial on how to use the methods.

## License ##

The code is published under the GNU General Public License. The code is provided "as is" and has no warranty whatsoever. 

## Data  ##

[imagingdata.mat (90 MB)](https://bitbucket.org/mackelab/gp_maps/downloads/imagingdata.mat)
Imaging data (optical imaging of intrinsic signals) used in the paper. The data is already normalized, and consists of one four-dimensional array of dimensions 126 by 252 by 8 by 100. The first two dimensions are pixels, the third dimension stimulus conditions, and the fourth dimension trials.

## References ##

J. H. Macke, S. Gerwinn, M. Kaschube, L. E. White, and M. Bethge 

Bayesian estimation of orientation preference maps

Advances in Neural Information Processing Systems 22, 2009, 

[pdf](https://bitbucket.org/mackelab/gp_maps/downloads/NIPS2009-Macke_6121.pdf)

J. H. Macke, S. Gerwinn, L. White, M. Kaschube, and M. Bethge 

Gaussian process methods for estimating cortical maps

NeuroImage, 2010

[pdf](https://bitbucket.org/mackelab/gp_maps/downloads/Macke_et_al_2010.pdf)