%This script illustrates the GP-methods for statistical modelling of
%imaging data and estimating orientation preference maps from them,
%as described in
%
%Macke et al, Gaussian process methods for estimating cortical maps, Neuroimage.

%
%
%This script generates a synthetic orientation preference map and synthetic imaging.
%Then, an orientation preference map is estimated from the data by

%%
clc
close all
clear all
addpath ./allfunctions
addpath ./GPs/LowRank
addpath ./GPs/ModelSelection
addpath ./GPs/Noise



%% Generating a synthetic map and synthetic imaging measurements

%Specify size of synthetic map in x and y direction, and the number of
%trials that should be generated
pixx=40;
pixy=40;
numtrials=20;


%generate responses to 8 different stimulus directions, in 45 degree
%increments:
orientations=[0:7]*pi/4;
%parameters for synthetic map: Filter white noise with a difference of
%Gaussians. The standard deviation of the positive Gaussian is
%hyperparams(1), of the negative Gaussian is hyperparams(2), and the
%amplitude of each of the two Gaussians is 10.
hyperparams=[3,6,1];

%The noise of the artifical data is generated using a factor-analysis noise
%models with datanoiseops.q dimensions, an average noise-variance of the correlated part
%of datanoiseops.noiselvel_corr, and an average variance of the independent part of
%datanoiseops.noiselevel_indep. (I.e. the bigger the two numbers are, the more noisy the data.
%The bigger the first number relative to the second, the more correlated
%the noise is.)
datanoiseops.model='factoran';
datanoiseops.q=2;
datanoiseops.noiselevel_indep=1;
datanoiseops.noiselevel_corr=.5;


%set random seed to ensure that same map is generated each time
randn('seed',0)
%generate toy data:
[xblock,allstims,truemap,datanoiseops]=MakeToyData(pixx,pixy,orientations,numtrials,hyperparams,datanoiseops);
%estimate map by vector averaging
[rawmap, smoothmap]=CalcMap(xblock, 2,  0,2);
%stop

%Now, we attempt to match the radial component of the auto-correlation
%function of the empirical map:
%We need to select a starting value for the estimation of the
%model-parameters. The optimization is nonlinear, so being in the right
%ballpark ist important.
priorops.beta0=[5,6,3];
fitdetails=MatchRadialComponents(xblock,priorops.beta0);
priorops.kerfunparams={fitdetails.beta1};
%reformat the data to get it in the format the GP-functions need:
[x,y,stims]=ReshapeData(xblock(:,:,:,1:numtrials));

%%
figure(1)
subplot(2,2,1)
showmap(truemap,'shadetowhite',-10,-90,[],0,'k');
title('Synthetic Orientation preference map')

subplot(2,2,2)
showmap(rawmap,'shadetowhite',-10,-90,[],2,'k');
title('Map obtained by vector-averaging the noisy data')

subplot(2,2,3)
showmap(smoothmap,'shadetowhite',-10,-90,[],0,'k');
title('Map obtained by smoothing the vector average')

subplot(2,2,4)
plot(fitdetails.xlags,fitdetails.rot_cov_all,'b.')
hold on
plot(fitdetails.xlags,fitdetails.K0,'g')
plot(fitdetails.xlags,fitdetails.K1,'k')
legend('data','initial parameters','fitted parameters')
title('Auto-correlation of the data and DoG-fit')

%asdfs

%% Now, set up parameters for the model itself
%datanoiseops.noisedata=[];
%We first assume that all pixels are observed
observed=ones(size(truemap))==1;

%Set parameters for the prior
priorops.maxq=800; %use approximation of  dimensionality 800
priorops.pixx=pixx;
priorops.pixy=pixy;


%Train three different models: One with independent noise model, one with
%factor analysis of dimensionality 2, and one with factor analysis of
%dimensionality 4
fprintf('\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n')
for kk=1:3
    fprintf('\n Train model %g',kk);
    switch kk
        case 1
            noiseops.model='indep';
            %now, train independent model
        case 2
            noiseops.model='factoran';
            noiseops.q=2;
        case 3
            noiseops.model='factoran';
            noiseops.q=4;
    end
    model{kk}=LearnGP(x,observed,y,stims,priorops,noiseops);

    if kk==1
        %do not re-calculate the prior for each model
        priorops.Prior=model{1}.Prior;
    end
        %delete some stuff that we dont really need:
        model{kk}=EfficientResults(model{kk});
    


end



%%
%%
figure(2)
subplot(2,2,1)
showmap(truemap,'shadetowhite',-10,-90,[],0,'k');
title('Synthetic Orientation preference map')

subplot(2,2,2)
showmap(model{1}.Posterior.map,'shadetowhite',-10,-90,[],2,'k');
title('Posterior mean of model 1')

subplot(2,2,3)
showmap(model{2}.Posterior.map,'shadetowhite',-10,-90,[],2,'k');
title('Posterior mean of model 2')

subplot(2,2,4)
showmap(model{3}.Posterior.map,'shadetowhite',-10,-90,[],2,'k');
title('Posterior mean of model 3')



