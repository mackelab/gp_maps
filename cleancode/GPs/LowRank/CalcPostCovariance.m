function Y=CalcPostCovariance(is,js,Prior,Noise,N,observed);
%calculate Posterior Covariance between data-points i and j (i and j could
%be a list of numbers each. 

M=size(Prior.G,1);
ei=sparse(M,numel(is));
for k=1:numel(is)
    ei(is(k),k)=1;
end
ej=sparse(M,numel(js));
for k=1:numel(js)
    ej(js(k),k)=1;
end

L=PreMultiByPostCovAllin(ej,N,Prior,Noise,observed);
Y=ei'*L;

%keyboard


