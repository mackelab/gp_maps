function mu_post=CalcPostMean(rhat,N,Prior,Noise,observed);

%keyboard

%first, left divide by Sigma_noise
%keyboard
rhat2=LowRankLeftDiv(rhat,Noise.D,Noise.G,Noise.InvR);
%then, left multiply by Koo;
%keyboard

mu_post=PreMultiByPostCov(rhat2,N,Prior,Noise,observed);

%keyboard
%first, premultiply by Sigma_noise^{-1};
%y=DplusGRHDivX(y,Noise.D,Noise.G,Noise.R);
%then, premultiply by K
%y=DplusGRHMultX(y,Prior.D,Prior.G,Prior,R);
%then, premultiply by (alphaSigma_noise+D)^{-1}, 
%where alpha=N/2;
%alpha=N/2;
%y=DplusGRHDivX(y,alpha*Noise.D+Prior.D,Noise.G,Noise.R);
%premultiply by G'
%y=Prior.G'*y;
%premult by (R^{-1}+G'*(alpha\Sigma_noise+D) G)^{-1}
%y=(inv(Prior.R)+Prior.G'*DplusGRHMultX(Prior.G,alpha*Noise.D+Prior.D,Noise.G,Noise.R))\y;
%by G
%y=Prior.G*y;
%add one, premult by alphaSigmaetc
%y=1+y;
%y=DplusGRHDivX(y,alpha*Noise.D+Prior.D,Noise.G,Noise.R);
%y=1-y;
%

