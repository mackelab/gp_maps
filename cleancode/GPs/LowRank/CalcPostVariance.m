function Y=CalcPostVariance(N,Prior,Noise,blocksize,observed);


%keyboard

M=numel(Prior.D);
numblocks=ceil(M/blocksize);
Y=zeros(M,1);

bige=spdiags(ones(M,1),0,M,M);

for k=1:numblocks;
    index=(1+blocksize*(k-1)):min(M,blocksize*k);
    e=full(bige(:,index));
   % keyboard
    L=PreMultiByPostCovAllin(e,N,Prior,Noise,observed);
    Y(index)=diag(e'*L);
end
