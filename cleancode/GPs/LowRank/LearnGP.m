function stuff=LearnGPMap(x,observed,y,stimuli,priorops,noiseops)
%function stuff=LearnGPMap(x,observed,y,stimuli,priorops,noiseops)
%
%Estimation of orientation preference maps from optical imaging data, as
%described in 
%
%Macke et al, Gaussian process methods for estimating cortical maps, under
%revision at Neuroimage.
%
%inputs:
%x: the coordinates at which the map should be calculated, a 2 by numpixels vector,
%where the k-th line is the x and y coordinate of the k-th pixel
%
%observed: a logical vector of length numpixels, with ones for those pixels
%at which data was observed, and zeros for those pixels for which no data
%was observed, but the map should be inferred nevertheless
%
%y: the experimental data, of size numpixel by numtrials (is called r in the paper)
%
%stims: the orientations of the stimuli, a vector of length numtrials
%
%priorops: A struct describing the options used for generating and
%calculating the prior. If a field is missing, the default value is used
%
%priorops.pixx and priorops.pixy: The size of the map, no default values at
%the moment (can be made obsolete, really, the code should also work on
%data which does not assume a rectangular map, but this is not tested)
%
%priorops.kerfun: A function handel for the functional shape of the kernel
%function. At the moment, only '@MexicanHatKernel' is tested. Default:
%@MexicanHatKernel
%
%priorops.kerfunparams: The hyper-parameters of the kernel-functio, no
%default parameters for that
%
%priorops.maxq: Maximal dimensionality of the low-rank approximation of the
%prior. Default is 1000.
%
%priorops.tol: Tolerance for incomplete cholesky decomposition. If
%reconstruction is better than 'tol', the decomposition is stopped, even if
%the dimensionality of the low-rank approximation is less than
%priorops.maxq. Default is .001
%
%priorops.ridge: Magnitude of the ridge added to the prior to ensure
%numerical stability, in percentage of the diagonal terms. Default is .01
%
%priorops.type='inc_chol': Use incomplete cholesky decomposition to
%approximation prior, only implemented method at the moment
%
%priorops.truelowrank: Use a prior which is either low rank, i.e. of form
%G*G', or low rank + ridge, i.e. of form G*G'+D, where D is diagonal.
%Default is false, i.e. use a ridge
%
%priorops.numcomponents: Number of components used in the estimation (order
%is cos(2*theta), sin(2*theta), cos(theta), sin(theta), sqrt(2),
%cos(3*theta), sin(3*theta), cos(4*theta)
%
%priorops.calcpostvar: Whether or not to calculate the posterior variance
%(which takes a long time to calculate, default ist false
%
%noiseops: A structur describing the options used for the noise-model. If a
%field is missing, the default value is used:
%
%noiseops.iterations: Iterate between estimating noise-model and posterior
%mean, default is 3
%
%noiseops.model: Model to use for estimation, at the moment, 'indep' and
%'factoran' are implemented and tested, default is 'factoran'. 
%
%noiseops.q: Use factor analysis model of dimensionality 2 (default). 
%
%noiseops.precision, ridge, maxiters and W_scaler: Parameters for factoranalysis,
%defaults are .01, .02, 500, .9
%
%noiseops.saveall: Save lots of intermediate results in the output or not,
%default is no, i.e. false
%
%
%outputs:
%stuff, a struct with most important fields
%.Posterior.map: The posterior mean, reshaped to be a complex valued map
%.Posterior.mean: All components of the posterior
%
%and further fields
%
%.stimuli: a vector of stimuli used
%.d the number of components used
%.x: the coordinates of the pixels
%.N: the number of total stimulus presentations
%.observed: which pixels are observed
%
%.Prior: the representation of the prior, namely: 
%.Prior.G: the low-rank approximating matrix G, of size numpixels by
%priorops.q
%.Prior.Pvec, .scores, .time_taken, .CholR, .InvR: info from the incomplete cholesky
%decomposition
%.Prior.q: Var_uncorrected, Var_exact: diagonal of low-rank approximation, and
% variance after correction with ridge
%.Prior.D: the correction ridge (is 0 if priorops.truelowrank=true) 
%
%.Posterior: representation of the posterior, namely:
%.Posterior.G: low-rank approximation to posterior covariance
%.N number of stimuli used 
%.rawmap: the empirical map, i.e. without smoothing, just vector averaging 
%.mean: the posterior mean
%.map: the map, reshaped to be complex valued and a matrix, such that one
%can look at it using showmap(stuff.Posterior.map)
%
%.Noise: 
%.IterNoiseVars: the noise variances after each iteration 
%.locNoises: the noise model after each iteration
%.G: the correlated noise component
%.D:the independent noise component
%.M the number of pixels
%
%this function calls the function chol_custom, which is based on the
%function chol_inc_fun by Francis Bach, 2002
%
%JHM 03/2010


[priorops,noiseops]=SetDefaultParams(priorops,noiseops);

M=size(x,1);
if isempty(observed)
    observed=logical(ones(M,1));
end

unobserved=~observed;
blocksize=100;
Mtest=sum(unobserved);
Mlearn=M-Mtest;
N=size(y,2);
stuff.stimuli=stimuli;
stuff.d=priorops.numcomponents;


%first, prepare prior decomposition over both x and x*. We only deal with the
%prior for the real part, the imaginary part is taking to be exactly the
%same
tic, fprintf('\n Prepare Low rank decomposition of prior ')
if isfield(priorops,'Prior')
    Prior=priorops.Prior;
    priorops.Prior=[];
    % keyboard
else
    Prior=LowRankPrior(x,priorops);
    %    keyboard
end
fprintf('\n Using low-rank approximation of dimensionality %g \n',Prior.q);
toc

for k=1:noiseops.iterations
    tic
    fprintf('\n Iteration %g, ',k)
    if k==1
        locNoise=LearnNoiseModel(x(observed,:),y,stimuli,noiseops);
    else
        locNoise=LearnNoiseModel(x(observed,:),y,stimuli,noiseops,Posterior.mean(observed,:));
    end
    locNoise.observed=observed;
    IterNoiseVars(:,k)=locNoise.D;
    if ~noiseops.saveall
        locNoises=[];
    else
        locNoises{k}=locNoise;
    end
    %    keyboard
    Posterior=SubspacePosterior(Prior,locNoise,N,observed);
    % Posterior=[];
    %keyboard
    Posterior.rawmap=MapClassicalHelper(y,stimuli)*N;
    Posterior.mean=CalcPostMean(Posterior.rawmap,N,Prior,locNoise,observed);
    IterPostMeans(:,:,k)=Posterior.mean;
end

Noise=locNoise;
Noise.locNoises=locNoises;
Noise.IterNoiseVars=IterNoiseVars;
Noise.IterPostMeans=IterPostMeans;


Posterior.map=reshape(Posterior.mean(:,1),priorops.pixx,priorops.pixy)+i*(reshape(Posterior.mean(:,2),priorops.pixx,priorops.pixy));
if  priorops.calcpostvar
    tic
    fprintf('\n Calculating Posterior Variance,\n')
    [Posterior.var]=CalcPostVariance(N, Prior, Noise, blocksize,observed);
    toc
end



stuff.x=x;
stuff.y=y;
stuff.N=N;
stuff.observed=observed;
stuff.Prior=Prior;
clear Prior
stuff.Noise=Noise;
clear Noise
stuff.Posterior=Posterior;








