function [y,innerblock]=LowRankLeftDiv(x,D,G,invR,H)
%
%calculate (D+G R H') ^{-1} x for diagonal or sparse square D (possibly given as vector), and G of
%size N times n, where N>> n, R is n times n, and H is n times N. If H is
%empty or not given, then H=G'; Not optimal yet, could be better.


[N,n]=size(G);
if (size(D,1)==1) ||  (size(D,2)==1);
    D=spdiags(D,0,N,N);
end

if nargin<=4 || isempty(H)
    %in this case, defining H explicitly is a waste of space, not really
    %necessary, could get rid of it if space runs out...
    H=G;

end

%keyboard

%keyboard
blocksize=1000;
if size(x,2)<=blocksize
    y=D\x;
    y=H'*y;
else
    y=zeros(size(x));
    y2=zeros(size(H'));
    numblocks=ceil(size(x,2)/blocksize);
    for k=1:numblocks
        index=(k-1)*blocksize+1:min(k*blocksize,size(x,2));
        y(:,index)=D\x(:,index);
        y2(:,index)=H'*y(:,index);
    end
    y=y2;
end

%keyboard

if size(G,2)<=blocksize
    innerblock= D\G;
    innerblock=invR+H'*innerblock;
    clear H
else
    numblocks=ceil(size(G,2)/blocksize);
    innerblock=zeros(size(G));
    for k=1:numblocks
        index=(k-1)*blocksize+1:min(k*blocksize,size(G,2));
        innerblock(:,index)=D\G(:,index);
    end
    innerblock=invR+H'*innerblock;
    clear H
end


if size(x,2)<=blocksize
    y=innerblock\ y;
    y=G*y;
    y=D\(x-y);
else
    numblocks=ceil(size(x,2)/blocksize);
    y2=zeros(size(x));
    for k=1:numblocks
        index=(k-1)*blocksize+1:min(k*blocksize,size(x,2));
        y2(:,index)=G*(innerblock\y(:,index));
        y2(:,index)=D\(x(:,index)-y2(:,index));
    end
    y=y2;
end

