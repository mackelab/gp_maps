function [y,innerblock]=LowRankLeftDivDouble(x,D,G1,invR1,H1,G2,invR2,H2)
%
%calculate (D+G R H'+G2 R2 H2') ^{-1} x for diagonal or sparse square D (possibly given as vector), and G of
%size N times n, where N>> n, R is n times n, and H is n times N. If H is
%empty or not given, then H=G'; Not optimal yet, could be better.
%function needs inv(R), not R.


[N,n]=size(G1);


if isempty(H1)
    %in this case, defining H explicitly is a waste of space, not really
    %necessary, could get rid of it if space runs out...
    H1=G1;
end
if isempty(H2);
    H2=G2;
end
%keyboard
y=LowRankLeftDiv(x,D,G1,invR1,H1);
y=H2' * y;
innerblock=invR2+ (H2'* LowRankLeftDiv(G2,D,G1,invR1,H1));
y=innerblock\y;
y=G2*y;
y=x-y;
y=LowRankLeftDiv(y,D,G1,invR1,H1);

