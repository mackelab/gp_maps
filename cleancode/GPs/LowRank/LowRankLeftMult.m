function y=DplusGRHMultX(x,D,G,R,H)
if nargin==4
    H=G;
end
if isvector(D) 
    D=spdiags(D,0,size(G,1),size(H,1));
end
y=G*(R*(H'*x));
%keyboard
y=D*x+y;

%keyboard
