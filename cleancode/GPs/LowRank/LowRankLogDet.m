function [L,Lbla,LW,LA]=LowRankLogDet(A,U,W,V)
%A is assumed to be a vector, corresponding to the diagonal of a diagonal
%matrix A, W is assumed to be of small dimensionality

%calculate log determinant of M=D+G*R*H', where invR=inv(R);
%essentially does 
%logdet(A+UWV')=logdet(W^{-1}=V'A^{-1}U)+logdet(W)+logdet(A);
n=length(A);
m=size(U,2);

Lbla=logdet(inv(W)+ V'*(spdiags(A.^(-1),0,n,n)*U));
LW=logdet(W);
LA=sum(log(A));
L=full(Lbla)+LW+LA;