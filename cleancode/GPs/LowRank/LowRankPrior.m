function Prior=LowRankPrior(x,ops)
%function Prior=LowRankPrior(x,ops)
%
%given data-points x, a kernel function ops.kerfun with parameters
%ops.ops.kerfunparams, and options lowrankops, construct an approximation prior
%covariance of the form K=G R G'+D;
%
%
%this function calls the function chol_custom, which is based on the
%function chol_inc_fun by Francis Bach, 2002
%
%options are described in the function LearnGP
%
%JHM 03/2010
%
%



switch ops.type
    case 'inc_chol'
        %evaluate diagonal 
        diagterm=ops.kerfun([0;0],[0;0],ops.kerfunparams{:});
        %get ridge in absolute terms
        ops.ridge=diagterm*ops.ridge/100;

        %do low-rank decomposition of prior (this is the time consuming
        %part)
        [Prior.G, Prior.Pvec,Prior.scores,Prior.time_taken] = chol_custom_constrained(x',ops.maxq,ops.tol,ops.ridge,ops.kerfun,ops.kerfunparams,ops.silent);
        %sort indices
        [a,Pvec]=sort(Prior.Pvec); Prior.G=Prior.G(Pvec,:);
        %get actual dimensionality (could be smaller than max)
        
        Prior.q=size(Prior.G,2);
        
        %the matrix R is a unit matrix, just for generality
        
        Prior.R=speye(Prior.q,Prior.q); %matrix R is unit matrix in this case
        %we find Prior.D by making sure that the prior variances for each pixel
        %are unbiased. Calculating this requires computation of the prior
        %variance for each pixel:
        
        %get diagonal of low-rank
        Prior.Var_uncorrected=zeros(size(x,1),1);
        Prior.Var_exact=zeros(size(x,1),1);
        for k=1:size(x,1);
            Prior.Var_uncorrected(k,1)=Prior.G(k,:)*Prior.G(k,:)';
            Prior.Var_exact(k,1)=ops.kerfun(x(k,:)',x(k,:)',ops.kerfunparams{:});
        end
        %add ridge to correct for it
        Prior.D=Prior.Var_exact-Prior.Var_uncorrected+2*ops.ridge;

    case {'fourier'}
        %Not really tested, might not work at all!!!
       % keyboard
        %first, calculate basis;

        [Gu,ops.FL,ops.smoothFL,ops.indices,ops.FLsparse,ops.D,ops.dropoff]=MakeFourierBasis(ops.maxq,ops.tol,ops.smoother,'kerfun',ops.pixx,ops.pixy,ops.kerfun,ops.kerfunparams);
        %then, calculate projection into basis. This should not be
        %necessary, but as I am stupid, I am including it at the moment.
        pix_smallfilter=20;
        [x,y]=meshgrid(-pix_smallfilter:pix_smallfilter,-pix_smallfilter:pix_smallfilter);
        xy=[x(:),y(:)];
        %        keyboard
        L=ops.kerfun(xy',0*xy',ops.kerfunparams{:});
        L=reshape(L,size(x,1),size(y,1));
        KtimesGu=Gu;
        Prior.L=L;
        for k=1:size(Gu,2);
            if mod(k,10)==0 && ~ops.silent
                PrintStar(k/10)
            end
            KtimesGu(:,k)=vec(conv2fft(reshape(KtimesGu(:,k),ops.pixx,ops.pixy),L,'same'));
        end
        %error('This method is not implemented yet')

        %Ru=Gu'*KtimesGu;
        %Ru=KtimesGu;
        

        %diagterm=mean(diag(Ru));
        %ops.ridge=diagterm*ops.ridge/100;
        %Ru=Ru+eye(size(Ru))*ops.ridge;

        %[Prior.Ruchol,Prior.Rcholindex]=cholincsp(Ru,ops.tol/1000);
        %[Prior.Ruchol,Prior.Rcholindex]=cholincfast(Ru,ops.tol/1000);
        %Prior.Rcholindex=sort(Prior.Rcholindex);
        
        %else
        %    [Prior.Ruchol,p]=chol(Ru)';
        %    Prior.Rcholindex=1:size(Prior.Ruchol,1);
        %end
%        Gu=Gu(:,Prior.Rcholindex);

 %       Ru=Ru(Prior.Rcholindex,Prior.Rcholindex);
 %       Prior.G=Gu*Prior.Ruchol;
 %       Prior.q=size(Prior.G,2);



    case 'none'
        %do nothing, save full covariance function

        K=zeros(size(x,1),size(x,1))
        for k=1:size(x,1)
            for kk=1:size(x,1);
                K(k,kk)=ops.kerfun(x(k,:),x(kk,:),ops.kerfunparams);
            end
        end
        [G]=chol(K);
        ops.q=size(x,1);
        prior.G=G;
        Prior.R=eye(ops.q);
        Prior.D=zeros(ops.q,1);
    otherwise
        error('No valid low-rank approximation method specified');
end


if ops.truelowrank
    Prior.D=Prior.D*0;
end



Prior.CholR=(chol(Prior.R));


Prior.InvR=(inv(Prior.R));


Prior.ops=ops;

