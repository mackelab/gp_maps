
function [G,FL,smoothFL,indices,FLsparse,D,dropoff]=MakeFourierBasis(q,tol,smoother,style,pixx,pixy,kerfun,kerparams)

switch style
   % case 'spec'
   %     FL=varargin{1};
    case 'kerfun'
        %pixx=varargin{1};
        %pixy=varargin{2};
        %kerfun=varargin{3};
        %kerparams=varargin{4};
        %only works for odd number of pixels at the moment
        [x,y]=meshgrid(-pixx/2+1/2:pixx/2-1/2,-pixy/2+1/2:pixy/2-1/2);
        xy=[x(:),y(:)];
%keyboard
kerparams=kerparams{:};
L=kerfun(xy'*0,xy',kerparams);
        L=reshape(L,size(x,1),size(x,2));
        FL=fftshift(fft2(L));
       
end


absFL=abs(FL);
if ~isempty(smoother) && smoother>0
    % keyboard
    smoothFL=conv2(absFL,GaussFilter(min(10,4*smoother),smoother),'same');
    %    keyboard
else
    smoothFL=absFL;
end



absFL=abs(FL);
[FLsorted,FLsortindex]=sort((vec((smoothFL))),'descend');
%FLsorted=FL(FLsortindex);

%tot=sum(absFL);
dropoff=cumsum(abs(FLsorted));
tot=sum(abs(FLsorted));
cutoff=tot*(100-tol)/100;
q=min(q,nnz(dropoff<=cutoff));
FLsorted=FLsorted(1:q);
FLsortindex=FLsortindex(1:q);

%keyboard

counter=0;
FLsparse=FL*0;
Z=FL*0;
G=zeros(pixx*pixy,q);
for k=1:min(q,numel(FLsorted))
    %if mod(k,20)==0
    %    PrintStar(k/20);
    %end
    Z=Z*0;
    [sx,sy]=ind2sub(size(FL),FLsortindex(k));
    Z(sx,sy)=1;
    %Z(end-sx+1,end-sy+1)=i;
    % imagesc(abs(Z))
    %keyboard
    A1=ifft2(fftshift(Z));
    A1=real(real(A1)+imag(A1));
    an=norm(A1(:));
    if an>1e-20
        counter=counter+1;
        A1=A1/an;
        G(:,counter)=vec(A1');
        indices(counter,:)=[sx,sy];
        D(counter)=(FLsorted(counter))*an;
        FLsparse(sx,sy)=FLsorted(counter);
        ann(counter)=an;
    end
end
G=G(:,1:counter);



