function [map]=MapClassicalhelper(y,stimuli,numcomponents);
%helper function as part of GP regression, nothing else, use CalcMap for
%getting 'actual' map;

if nargin==2
    numcomponents=5;
end

[M,N]=size(y);

stimuli=vec(stimuli)';
map=zeros(M,8);

v1=cos(2*stimuli);
%keyboard
map(:,1)=mean(y.*tile(v1,y),2);

v2=sin(2*stimuli);
map(:,2)=mean(y.*tile(v2,y),2);

v3=cos(stimuli);
map(:,3)=mean(y.*tile(v3,y),2);

v4=sin(stimuli);
map(:,4)=mean(y.*tile(v4,y),2);

v5=0*stimuli+sqrt(.5);
map(:,5)=mean(y.*tile(v5,y),2);

v6=cos(stimuli*3);
map(:,6)=mean(y.*tile(v6,y),2);

v7=sin(stimuli*3);
map(:,7)=mean(y.*tile(v7,y),2);

v8=cos(stimuli*4);
map(:,8)=mean(y.*tile(v8,y),2);

map=map(:,1:numcomponents);


%map(:,5)=mean(y.*tile(v5,y),2);

