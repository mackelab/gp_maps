function y=PreMultiByPostCov(x,N,Prior,Noise,observed);
%given a prior distribution and noise model, premultiply x by the posterior
%covariance. It is assumed that the vector x indexes into the posterior
%covariance via the binary vector x, i.e. in fact, it computes
%PostCov(:,observed)*x;
%keyboard
%keyboard
y=LowRankLeftMult(x,Prior.D(observed),Prior.G(observed,:),Prior.R);
alpha=2/N;
%then, left divide by alphaSigmanoise+Do+GoRoGo'
y=LowRankLeftDivDouble(y,Prior.D(observed)+alpha*Noise.D,Noise.G,1/alpha*Noise.InvR,[],Prior.G(observed,:),Prior.InvR,[]);
%subtract off from rhat2

%keyboard

y=x-y; 
%now, left multiply by  K*o
%keyboard
y=LowRankLeftMult(y,Prior.D(observed), Prior.G,Prior.R,Prior.G(observed,:));

%keyboard

%keyboard
