function y=PreMultiByPostCovAllin(x,N,Prior,Noise,observed);
%given a prior distribution and noise model, premultiply x by the posterior
%covariance. It is assumed that the noise covariance is only over the
%observed data-points.

if nargin==4
    observed=Noise.observed;
end

%keyboard
y1=LowRankLeftMult(x,Prior.D(observed),Prior.G(observed,:),Prior.R,Prior.G);
alpha=2/N;
%then, left divide by alphaSigmanoise+Do+GoRoGo'
y1=LowRankLeftDivDouble(y1,Prior.D(observed)+alpha*Noise.D,Noise.G,1/alpha*Noise.InvR,[],Prior.G(observed,:),Prior.InvR,[]);
%subtract off from rhat2

y1=LowRankLeftMult(y1,Prior.D(observed), Prior.G,Prior.R,Prior.G(observed,:));


y2=LowRankLeftMult(x,Prior.D,Prior.G,Prior.R);

y=y2-y1;
