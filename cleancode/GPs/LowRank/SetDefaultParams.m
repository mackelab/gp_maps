function [priorops,noiseops]=SetDefaultParams(priorops,noiseops);
%function [priorops,noiseops]=SetDefaultParams(priorops,noiseops);
%
%Set default parameters for prior-options and noise-options
%
%JHM 03/2010

pairsprior={'maxq',1000;'kerfun',@MexicanHatKernel; 'tol',.001;'ridge',.01;...
    'type','inc_chol';'truelowrank',false;'numcomponents', 5; 'calcpostvar', false; 'silent',false};

pairsnoise={'iterations',3;'model','factoran';'q',2;'precision',.01;'ridge',.02;...
    'maxiters',500;'W_scaler',.9;'saveall',false};


for k=1:size(pairsprior,1);
    if ~isfield(priorops,pairsprior{k,1}) 
        priorops=setfield(priorops,pairsprior{k,1},pairsprior{k,2});
    end
end

for k=1:size(pairsnoise,1);
    if ~isfield(noiseops,pairsnoise{k,1}) 
        noiseops=setfield(noiseops,pairsnoise{k,1},pairsnoise{k,2});
    end
end
    
