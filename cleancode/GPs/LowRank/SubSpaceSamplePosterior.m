function [maps,p,imagmaps]=SamplePosterior(Posterior,numsamples,pixx,pixy);
%sample maps from posterior



mu=Posterior.mean;
if ndims(mu)==3;
    mu=reshape(mu,size(mu,1)*size(mu,2),size(mu,3));
end

%keyboard
L=chol(Posterior.M)';
d=size(Posterior.M,1);
GG=Posterior.G*L;
for k=1:numsamples;
 %   keyboard
 W=randn(d,size(mu,2));
    A=GG*W;
   maps(:,:,k)= A+mu;
   %keyboard
   p(k,:)=sum(log(normpdf(W)));
end





if nargin>=3
  %  keyboard
    pixy=size(maps,1)/pixx;
    maps=reshape(maps,pixx,pixy,size(maps,2),size(maps,3));
    imagmaps=squeeze(maps(:,:,1,:)+i*maps(:,:,2,:));
end
