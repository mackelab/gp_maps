function [maps,p,imagmaps]=SubspaceSamplePrior(Prior,numsamples,pixx);
%sample maps from posterior



%if ndims(mu)==3;
%    mu=reshape(mu,size(mu,1)*size(mu,2),size(mu,3));
%end

L=chol(Prior.R)';
q=size(Prior.R,1);
d=5;
GG=double(Prior.G)*double(L);
for k=1:numsamples;
 %   keyboard
 W=randn(q,d);
    A=GG*W;
   maps(:,:,k)= A;%+mu;
   %keyboard
   p(k,:)=sum(log(normpdf(W)));
end





if nargin>=3
  %  keyboard
    pixy=size(maps,1)/pixx;
    maps=reshape(maps,pixx,pixy,size(maps,2),size(maps,3));
    imagmaps=squeeze(maps(:,:,1,:)+i*maps(:,:,2,:));
end
