function Posterior=SubspacePosterior(Prior,Noise,N,observed)
%if the prior has low rank, so does the posterior. Therefore, in this case,  the posterior
%can be characterized simply by a matrix M which is of size maxd by maxd.
%However, if that is the case, we have to stop using priors of the form
%K=D+G*R*G', and switch to K=GR*G'
%keyboard
Posterior.G=double(Prior.G);
Prior.G=double(Prior.G);
Prior.R=double(Prior.R);
M=   double(Prior.G(observed,:))*double(Prior.R);
alpha=2/N;

%keyboard
Noise.G=double(Noise.G);
%keyboard
M= LowRankLeftDivDouble(M,alpha*Noise.D+Prior.D(observed),Noise.G,1/alpha*Noise.InvR,[],Prior.G(observed,:),Prior.InvR,[]);
%keyboard
M=Prior.G(observed,:)'*M;
M=eye(size(M))-M;
M=Prior.R*M;

Posterior.M=M;
Posterior.N=N;
