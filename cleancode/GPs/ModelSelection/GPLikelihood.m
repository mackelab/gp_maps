function [AvL,AllL]=GPLikelihood(model, testdata,teststims, selector,ops,numcomponents);

%evaluate likelihood of data under Gaussian Process Model, as a first step
%in decoding/encoding, etc.... uses only the dimensions specified in
%'selector', the other ones are ignored. If selector is empty of missing,
%all dimensions are taken. ops is 'map' or 'full', depending on whether the
%likelihood should be 'fully' bayesian or not.


[npix, nstims]=size(testdata);
testdata=testdata(selector,:);

mu=double(model.Posterior.mean(selector,:));
Dnoise=double(model.Noise.D(selector));
Gnoise=double(model.Noise.G(selector,:));
Rnoise=double(model.Noise.R);
InvRnoise=double(model.Noise.InvR);

logdetNoise=LowRankLogDet(Dnoise,Gnoise,Rnoise,Gnoise);


for k=1:nstims
    for kk=1:size(teststims,2);

        locdat=testdata(:,k);
        theta=teststims(k,kk);
        v=[cos(2*theta); sin(2*theta);cos(theta);sin(theta);sqrt(.5)];
        if numcomponents==3
            v(3:4)=0;
        end
        %v=v(1:numcomponents);

        locmean=mu*v;
       % keyboard
        
        locdat=locdat-locmean;

        AllL(k,kk)=-0.5*logdetNoise-0.5*locdat'*LowRankLeftDiv(locdat,Dnoise,Gnoise,InvRnoise);
       % keyboard
    end
    % keyboard
end

AllL=single(AllL/numel(selector));
AvL=mean(AllL);

%keyboard


