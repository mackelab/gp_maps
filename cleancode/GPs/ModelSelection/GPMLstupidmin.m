function [output]=GPMLstupidminp(x,observed,y,stimuli,priorops,noiseops,minops)
%learn GP on map with data-points x, measured values y, stimuli vs, prior
%covariance kerfun, which has to be a function handle kerfun(x1,x2,
%kerfunparams), and specified kernel parameters kerfunparams. Does
%lowrank-approximations as specified by priorops
% the posterior is returned over all of x, even if some x are unobserved
%
%in the future, also want a function that takes in directly not the
%kernel-function, but a power-spectrum and associated dimensionality
% returns
% mu_post, the posterior mean over points xtest
% var_post, the posterior variance over points xtest
% G,R,D the low-rank approximation to the prior, K= GRG'+D
%Ge, Re, De, he low-rank approximation to the noise, Sigma_e=Ge Re Ge'+De
%
%x has to be of size Mall by 2
%y is of size N by numel(stimuli)


M=size(x,1);
if isempty(observed)
    observed=logical(ones(M,1));
end

unobserved=~observed;

Mtest=sum(unobserved);
Mlearn=M-Mtest;
N=size(y,2);
stuff.stimuli=stimuli;
stuff.d=4;


%first, prepare prior decomposition over both x and x*. We only deal with the
%prior for the real part, the imaginary part is taking to be exactly the
%same
fprintf('\n Learning noise model, ')
Noise=LearnNoiseModel(x(observed,:),y,stimuli,noiseops);
Noise.observed=observed;
toc

ops=optimset('display','iter','MaxFunEvals',minops.maxfunevals,'TolFun',minops.tolfun);
x0=(minops.kerparams_init);

switch minops.method
    case 'fminsearch'
[kerfinal,fval,exitflag,output]=fminsearch(@(kerparams)MyObjective(kerparams,x,y,priorops,Noise),x0,ops);
    case 'fminunc'
[kerfinal,fval,exitflag,output]=fminunc(@(kerparams)MyObjective(kerparams,x,y,priorops,Noise),x0,ops);
    case 'fmin_grad'
        ops.GradObj='off';
        ops.DerivativeCheck='off';
        ops.Diagnostics='off';
        %keyboard
[kerfinal,fval,exitflag,output]=fminunc(@(kerparams)MyObjectiveGrad(kerparams,x,y,priorops,Noise),x0,ops);
end


output.kerfinal=(kerfinal);
output.fval=fval;
output.exitflag=exitflag;

function ML=MyObjective(kerparams,x,y,priorops,Noise);
priorops.kerfunparams={(kerparams)};
priorops.silent=1;
Prior=LowRankPrior(x,priorops);
ML=-LowRankEvidence(y,Prior,Noise);

function [ML,MLgrad]=MyObjectiveGrad(kerparams,x,y,priorops,Noise);
%keyboard
priorops.kerfunparams={kerparams};
priorops.silent=1;
Prior=LowRankPrior(x,priorops);
[ML,MLgrad]=LowRankEvidence2(y,Prior,Noise);
ML=-ML;
MLgrad=-MLgrad;

%keyboard




