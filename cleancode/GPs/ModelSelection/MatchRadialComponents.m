function [details]=MatchRadialComponents(xblock,beta0)
%function [details]=MatchRadialComponents(xblock,beta0)
%
%Little helper function to estimate the hyper-parameters (i.e. the
%parameters of the covariance function) by matching them to the radial
%component of the auto-correlation function of the empirically estimated
%map. This function is rather ad-hoc, and there is plenty of room for
%improvement by either making the fitting procedure less sensitive to
%initial conditions, or by finding a better loss function (e.g. marginal
%likelihood), or by being mor flexible in the specification of the
%kernel-function, and not insisting on it being a difference-of-Gaussians. 
%
%inputs: 
%xblock: A 4-D array of data, of size pixx by pixy by number stimuli (which
%are assumed to be equally spaced directions, and the function has only
%been really tested for 8 stimulus directions)
%
%outputs:
%One struct, details, with fields
%beta1: the parameters of the fitted covariance function
%rotcov_all: The empirical auto-covariance function
%xlogs: are the lags at which the empirical auto-covariance function is
%evaluated
%K0 and K1: The covaraince function with initial parameters beta0 and with
%fitted parameters beta1
%details.r,details.J,details.SIGMA,details.mse are the output-arguments of
%the matlab-function nlinfit.
%
%
%JHM 03/2010

maxlags=100;
[pixx,pixy,numconds,numtrials]=size(xblock);

[rawmap, smoothmap]=CalcMap(xblock, 2,  50, .5);
rawmap=smoothmap;
locdat=real(rawmap)-mean(vec(real(rawmap)));
covreal=double(xcorr2(locdat))/numel(locdat);

[rot_cov(:,1),xlags]=RotAVG(covreal,0:maxlags);

locdat=imag(rawmap)-mean(vec(imag(rawmap)));
covimag=double(xcorr2(locdat))/numel(locdat);
[rot_cov(:,2),xlags]=RotAVG(covimag,0:maxlags);

%keyboard
%xlags=xlags';
xlags=xlags(~isnan(rot_cov(:,1)));
rot_cov=rot_cov(1:numel(xlags),:);
rot_cov_all=mean(rot_cov,2)';





beta0small=[beta0(1),beta0(3)];

ops=statset;
minlag=1;
xlags=xlags+0;
[beta1,details.r,details.J,details.SIGMA,details.mse]=nlinfit(xlags(minlag:end),rot_cov_all(minlag:end),@(params,r)MexicanHatKernel_2(r,0,params),beta0small,ops);




details.K0=MexicanHatKernel(xlags,0,beta0);
details.K1=MexicanHatKernel_2(xlags,0,beta1);

details.xlags=xlags;
details.rot_cov_all=rot_cov_all;

L=(var(real(rawmap(:)))+var(imag(rawmap(:))))/2/MexicanHatKernel(0,0,[beta1(1),2*beta1(1),1]);

beta1(2)=1/10*beta1(2)+9/10*L;

details.beta1=[beta1(1),beta1(1)*2,beta1(2)];



%if 1
%plot(xlags,rot_cov_all,'b.')
%hold on
%plot(xlags,K0,'g')
%plot(xlags,K1,'k')
%end
