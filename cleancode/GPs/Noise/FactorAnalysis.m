function [W,Psi,scores,morestuff]=FactorAnalysis(t,m, maxiters,precision,ridge,W0,Psi0)
%function [W,Psi,scores,morestuff]=FactorAnalysis(t,m, maxiters,precision,ridge,W0,Psi0)
%
%factor analysis on the model
%t=Wx+eps for observed data y, where eps has diagonal noise variance psi,
%and x is white, and the loadings W are of size n by m
%maximum likelihood estimation of the loadings W via EM, and returns
%
%W, the matrix of loadings,
%beta, such that beta t is E(x|t)
%Psi, a vector of variances of eps
%
%optimized to work in high dimensions, but at the moment, only for small
%number of datapoints, although that could easily be fixed. This function
%is still a bit away from optimality, might have some residual bugs, 
%and does not seem to work in some
%situations, so it is to be used with care
%
%
%
%
%JHM 03/2010

[n,N]=size(t);


%sometimes, need to do some blocking of data-points to save memory
blocksize=50;
numblocks=ceil(N/blocksize);

%validate algorithm by comparing it to true covariance, but only do that
%for a computationally tractable subset
test_cov_size=min(200,n);
%rando=1:n;
rando=randperm(n);
test_index=rando(1:test_cov_size);
t_test=t(test_index,:);
truecov=(t_test*t_test')/N;
truecorr=OffDiag(Cov2Corr(truecov));
%initialize Psi and W
%Psi=ones(n,1);

if nargin<=5
    W=randn(n,m)/sqrt(n);
else
    W=W0;
end

var_t=mean(t'.^2)'+ridge;

%keyboard;

if nargin<=6
    Psi=var_t;
else
    Psi=Psi0;
end

if m==0;
    W=zeros(n,1);
    scores=nan;
    morestuff=[];
    return;
end


for k=1:maxiters

    PsiInv=spdiags(Psi.^1,0,n,n);
    PrintStar(k)
    testcov=W(test_index,:)*W(test_index,:)'+diag(Psi(test_index));
    testcorr=OffDiag(Cov2Corr(testcov));
    scores(k)=0;
    scores(k)=-N*n/2*log(2*pi)-N/2*logdet(eye(m)+W(test_index,:)'*PsiInv(test_index,test_index)*W(test_index,:))-N/2*sum(log(Psi));
    for kk=1:numblocks
        localindex=(kk-1)*blocksize+1:min(blocksize*kk,N);
        scores(k)=scores(k)-1/2*trace(t_test(:,localindex)'*LowRankLeftDiv(t_test(:,localindex), Psi(test_index), W(test_index,:), eye(m), W(test_index,:)));
    end
    scores(k)=-scores(k)/N/n;
    morestuff.corr_corr(k)=corr((truecorr),(testcorr));
    morestuff.corr_mse(k)=mean(((truecorr)-(testcorr)).^2);
    morestuff.cov_mse(k)=mean((OffDiag(truecov)-OffDiag(testcov)).^2);
    morestuff.var_corr(k)=corr(diag(truecov),diag(testcov));
    morestuff.var_mse(k)=mean((diag(truecov)-diag(testcov)).^2);
    a=polyfit((truecorr),(testcorr),1);
    morestuff.corr_scaling(k)=a(1);

    if (k>5) && abs(scores(k)-scores(k-1))<precision && abs(morestuff.corr_scaling(k)-morestuff.corr_scaling(k-1))<precision && abs(morestuff.cov_mse(k)-morestuff.cov_mse(k-1))<precision
        fprintf('\nLosses dont improve anymore, aborting at % g\n', scores(k));
        break
    elseif k>1 && scores(k)>scores(k-1)+precision*10
        fprintf('\nWarning, loss is actually increasing, from %g to %g \n', scores(k-1), scores(k));
    end


    PsiInv=spdiags(Psi.^1,0,n,n);
    beta=(eye(m)+W'*PsiInv*W)\(W'*PsiInv);
    A=t*(t'*beta');
    B=beta*A;
    A=A/N;
    B=B/N;

    B=eye(size(B))-beta*W+B;

    L=0;
    W=A/B;

    for kk=1:n
        Psi(kk)=A(kk,:)*W(kk,:)';
    end
    Psi=var_t-Psi;
    if min(Psi)<ridge
        warning('Psis are small')
    end
    Psi=max(Psi,ridge);
end
morestuff.truecov=truecov;
morestuff.testcov=testcov;
morestuff.var_t=var_t;

