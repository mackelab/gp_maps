function Noise=LearnNoiseModel(x,y,stimuli,ops,mu_post)
%function Noise=LearnNoiseModel(x,y,stimuli,ops,mu_post)
%
%Calculate Noise model for GP-estimation of orientation preference maps or
%other imaging data. 
%
%inputs:
%.model: 'indep' or 'factoran', for independent noise model or factor
%analysis model
%.q: Number of dimensions for correlated part G in the factor analyis model
%.maxiters: Maximum number of iterations for factor analysis
%.precision: Stopping criteration for the likelihood-changes in factor
%analysis
%.ridge: minimal values for diagonal in factor analysis
%.W_scaler: 'deflation' for correlated component in factor analysis (for
%numerical stability, a number between 0 and 1, better close to 1)
%
%outputs: the struct Noise with fields
%.G: The correlated components of the noise-covariance
%.D: the independent components of the noise-covariance
%.fit_details: details about the fitting of the factor analysis model
%.M, .q: number of stimuli and dimensionality of noise model
%.R (a matrix of ones, feel free to ignore)
%
%JHM 03/2010

M=size(x,1);
stimuli=round(1000*stimuli)/1000;
allstims=unique(stimuli);

if nargin<=4 %posterior mean not given, calculate usual noise correlations
    for k=1:numel(allstims)
        locmean=mean(y(:,stimuli==allstims(k)),2);
        Noise.locmeans(:,k)=locmean;
        y(:,stimuli==allstims(k))=y(:,stimuli==allstims(k))-tile(vec(locmean),y(:,stimuli==allstims(k)));
    end
elseif nargin==5 %posterior mean given, calculate standard deviations from mu_post
    for k=1:numel(allstims)
        locmean=mu_post*StimVec(allstims(k),size(mu_post,2));
        Noise.locmeans(:,k)=locmean;
        y(:,stimuli==allstims(k))=y(:,stimuli==allstims(k))-tile(vec(locmean),y(:,stimuli==allstims(k)));
    end
end


switch ops.model
    case 'indep'
        ops.q=1;
        NoiseVar=var(y,[],2);
        NoiseVar=max(NoiseVar,ops.ridge);
        D=double(vec(NoiseVar));
        G=sparse(M,ops.q);
        R=1;

    case 'factoran'
       % keyboard
        [W,Psi,scores,morestuff]=FactorAnalysis(double(y),ops.q, ops.maxiters,ops.precision,ops.ridge);     %   keyboard
       % keyboard
        W=W*ops.W_scaler;
        wvar=zeros(size(W,1),1);
        for k=1:size(W,1)
            wvar(k,1)=W(k,:)*W(k,:)';
        end
        Psi=morestuff.var_t-wvar;
          
        D=vec(Psi);
        G=W;
        morestuff.scores=scores;
        morestuff.G=[];
        morestuff.noisedata=[];
        ops.fit_details=morestuff;
        
        R=spdiags(ones(ops.q),0,ops.q,ops.q);
        
end


Noise.allstims=allstims;
Noise.ops=ops;
Noise.q=size(G,2);
Noise.M=M;
Noise.G=G;
clear G
Noise.R=R;
Noise.InvR=inv(R);
clear R
Noise.D=D;


