function [L,Ls]=NoiseLikelihood(NoiseModel,y,stimuli);

y=double(y);
stimuli=round(1000*stimuli)/1000;

L=0;
m=NoiseModel.q;
%keyboard
allstims=NoiseModel.allstims;
for k=1:numel(allstims)
    locmean=NoiseModel.locmeans(:,k);
    y(:,stimuli==NoiseModel.allstims(k))=y(:,stimuli==NoiseModel.allstims(k))-tile(locmean,y(:,stimuli==allstims(k)));
end


[n,N]=size(y);


Psi=NoiseModel.D;
W=NoiseModel.G;
PsiInv=spdiags(Psi.^1,0,n,n);

L=-1/2*(n*log(2*pi)+logdet(eye(m)+W'*PsiInv*W)+sum(log(Psi)));


for k=1:N
    Ls(k)=L-1/2*(y(:,k)'*LowRankLeftDiv(y(:,k), Psi, W, eye(m), W));
end

L=sum(Ls)/N/n;

