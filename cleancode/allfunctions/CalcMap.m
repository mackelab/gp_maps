function [rawmap, smoothmap]=CalcMap(dat,angles,  highpass_std, smooth_std)
%function [rawmap, smoothmap]=CalcMap(dat,angles,  highpass_std, smooth_std)
%
%calculate orientation preference map from raw data, and calculate a
%smoothed version of the map also
%
%inputs:
%
%dat: the raw dat, a 4-D array, of size pixx by pixy by numstimuli by
%numtrialspercondition, the imaging measurements for each stimulus and
%repetition
%
%angles: Either supply the angles used in the experiment as a vector (for
%orientation preference maps, double the angles) or set angles=2 to estimate
%an orientation preference maps with equally spaced angles
%
%highpass_std: Filter-width of the high-pass filter for the map,
%use zero for on high-pass filtering
%
%smooth_std: Filter-width of the low-pass filter for the smoothed map, uze
%zero for no filtering
%
%outputs:
%
%rawmap: estimated orientation preference maps
%
%smoothmap: Map after smoothing and high-pass filtering
%
%JHM 03/2010

[x,y,numconds,numtrials]=size(dat);

meandat=mean(dat,4);

totalmean=mean(meandat,3);
meandat=meandat-repmat(totalmean,[1,1,numconds]);

for k=1:numconds
    meandat(:,:,k)=meandat(:,:,k)-mean(vec(meandat(:,:,k)));
    if highpass_std>0
        meandat(:,:,k)=meandat(:,:,k)-conv2fft(meandat(:,:,k),GaussFilter(2*highpass_std, highpass_std),'same');
    end
end

if isempty(angles) | angles==1
    angles=linspace(0,2*pi, numconds+1);
    angles=angles(1:end-1);
elseif angles==2
    angles=linspace(0,2*pi, numconds+1);
    angles=angles(1:end-1);
    angles=2*angles;
end

rawmap=zeros(x,y);
for k=1:numconds
    rawmap=rawmap+1/32*8* exp(i*angles(k))*meandat(:,:,k);
end

if smooth_std>0
smoothmap=conv2fft(rawmap,GaussFilter(2*smooth_std, smooth_std),'same');
else
smoothmap=rawmap;
end

