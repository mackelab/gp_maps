function stuff=EfficientResults(stuff,agression)
%get rid of some fields that are memory expensive, and are not really
%needed.

%keyboard
if nargin==1
    agression=0;
    stuff.Prior=[];
    stuff.Posterior.G=[];
    stuff.y=[];
    stuff.Noise.IterNoiseVars=[];
    %stuff.Noise.locmeans=[];
    stuff.Noise.IterPostMeans=[];
    stuff.Noise.ops.G=[];
    stuff.Noise.ops.noisedata=[];
    try stuff.Posterior.mean=single(stuff.Posterior.mean); end
    try stuff.Posterior.rawmap=single(stuff.Posterior.rawmap); end
    try stuff.Posterior.var=single(stuff.Posterior.var); end
    try stuff.Posterior.M=single(stuff.Posterior.M); end
    try  stuff.Noise.G=single(stuff.Noise.G);  end
end





return

if strcmp(agression,'prior') || strcmp(agression,'priorops');
    %try stuff.Prior.G=single(stuff.Prior.G); end
    stuff.y=[];
    try  stuff.Prior.R=single(stuff.Prior.R); end
    try  stuff.Prior.CholR=single(stuff.Prior.CholR); end
    try  stuff.Prior.InvR=single(stuff.Prior.InvR); end
    return
end

%keyboard
if agression>=1
    stuff.Noise.G=[];
    stuff.x=[];
    stuff.y=[];
end
if agression>=2
    stuff.Posterior.G=[];
    stuff.Noise.R=[];
    stuff.Noise.InvR=[];
    stuff.Prior=[];
end
if agression>=3
    stuff.Posterior.M=[];
    stuff.locNoise=[];
    stuff.Prior=[];
    stuff.Noise=[];
end
