function h=GaussFilter(xsize,sigma,varargin)
if length(varargin)==0 | (length(varargin)==1 && strcmp(varargin{1},'2D'))
[x,y]=meshgrid(-xsize:xsize,-xsize:xsize);
h=exp(-(x.^2+y.^2)/2/sigma^2);

elseif strcmp(varargin{1},'1D')
x=-xsize:xsize;
h=exp(-x.^2/2/sigma^2);
elseif strcmp(varargin{1},'3D')
[x,y,z]=meshgrid(-xsize:xsize,-xsize:xsize,-xsize:xsize);
h=exp(-(x.^2+y.^2+z.^2)/2/sigma^2);
end
    
h=h/sum(h(:));
