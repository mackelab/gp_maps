function A=Lpfilter(xsize, decay, p);

[xx,yy]=meshgrid([-xsize:xsize],  [-xsize:xsize]);

r=sqrt(xx.^2+yy.^2);

A=exp(-(r/decay).^p);
A=A/sum(A(:));

%keyboard
