function [xblock,allstims,truemap,datanoiseops,allthenoise]=MakeToyData(pixx,pixy,orientations,numtrials,hyperparams,datanoiseops);

%function
%[xblock,allstims,truemap,datanoiseops]=MakeToyData(pixx,pixy,orientations,numtrials,hyperparams,datanoiseops);
%Simulate an imaging experiment by generating synthetic map and then
%samples from it
%
%inputs:
%pixx pixy: Size of map to be generated
%orientations: A vector of angles (in radians), the stimulus orientations
%for which responses should be simulated
%numtrials: The number of responses for each stimulus
%hyperparams: Parameters of the synethic map to be generated, a vector of
%three numbers, corresponding to the std of the positive Gaussian, the std
%of the negative Gaussian, and the amplitude of the filter
%noiseops: A struct describing how the noise should be generated
%noiseops.model:
%Use 'indep' for independent noise, the noiseopds.noisevar
%should be a matrix (or vector) of the point-wise noise variances for each
%pixel
%Use 'cov' if the whole noise-covariance is supplied (only works for small
%maps), noiseops.noisecov must then be the noise-covariance
%Use 'factoran' for a factor analysis model, in this case,
%noiseops.noisestd has to be a vector of standard-deviations for the
%independent noise component, and noiseops.G a matrix of the low-rank
%correlated noise components
%
%outputs:
%xblock is a 4-D array of the noisy simulated responses for each trial
%where xblock(:,:,a,b) is the response to the b-th repetition of the a-th
%stimulus
%allstims is a vector of stimuli
%truemap is the synthetic map that was generated
%datanoiseops is a struct of options that is being returned again
%
%also SyntheticDataFromMap CalcMap
%
%JHM 07/2011
%%%%%%%%%%%%

if isempty(hyperparams)
    hyperparams=[6,12,2.1];
end


[x,y]=meshgrid(1:pixx,1:pixy);


W=randn(pixx,pixy,4);
h1=GaussFilter(50,hyperparams(1,1)); h2=GaussFilter(50,hyperparams(1,2));
hh=(h1-h2)*sqrt((hyperparams(3)));
for kk=1:2
    mapsconv(:,:,kk)=conv2fft(W(:,:,kk),hh,'same');
end



noisefilt=Lpfilter(50,.5,.5);

datanoiseops.D=conv2fft(randn(pixx,pixy),noisefilt,'same');
datanoiseops.D=abs(datanoiseops.D);
datanoiseops.D=datanoiseops.D(:)/sqrt(mean(datanoiseops.D(:).^2))*datanoiseops.noiselevel_indep;

%datanoiseops.q=2;
%datanoiseops.noiselevel_indep=1;
%datanoiseops.noiselevel_corr=.5;



switch datanoiseops.model
    case 'factoran'
        for k=1:datanoiseops.q
            l=conv2fft(randn(pixx,pixy),noisefilt,'same');
            l=l(:);
            l=l/sqrt(mean(l.^2))*datanoiseops.noiselevel_corr/sqrt(datanoiseops.q);
            datanoiseops.G(:,k)=l(:);
        end
end


%keyboard
truemap=mapsconv(:,:,1)+i*mapsconv(:,:,2);
%keyboard


[xblock,allstims,allthenoise]=SyntheticDataFromMap(truemap, orientations, numtrials, datanoiseops);
