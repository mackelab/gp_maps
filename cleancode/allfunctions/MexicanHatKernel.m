function [K,K2logsigma1,K2logsigma2,Kalpha]=MexicanHatKernel(x,y,hyperparams);
%Mexican Hat Kernel function with parameters hyperparams(1)==width of
%positiv Gaussian, hyperparams(2)=width of negative Gaussian,
%hyperparams(3)=overall scaling


D=sum((x-y).^2,1);


%if numel(hyperparams)==2
%sigma=hyperparams;
%alphas=[1,-1];
hyperparams(1)=hyperparams(1)*hyperparams(1);

hyperparams(2)=hyperparams(2)*hyperparams(2);

D1=exp(-1/4*D/hyperparams(1))/pi;
D2=exp(-1/4*D/hyperparams(2))/pi;
Dcross=exp(-1/2*D/(hyperparams(1)+hyperparams(2)))/pi;
K=0.25/hyperparams(1)*D1+0.25/hyperparams(2)*D2-1/(hyperparams(1)+hyperparams(2))*Dcross;
K=K*hyperparams(3);

if nargout>1
    alpha=hyperparams(3);
    sigma1s=hyperparams(1);
    sigma2s=hyperparams(2);
    beta=log(hyperparams(1));
    gamma=log(hyperparams(2));
    Kalpha=K*alpha;
    K2logsigma1= D1*alpha/4/sigma1s.*(D/4/sigma1s-1)+Dcross*alpha*sigma1s/(sigma1s+sigma2s)^2.*(1-0.5/(sigma1s+sigma2s).*D);
    K2logsigma2= D2*alpha/4/sigma2s.*(D/4/sigma2s-1)+Dcross*alpha*sigma2s/(sigma1s+sigma2s)^2.*(1-0.5/(sigma1s+sigma2s).*D);

end

%else
%    K=0;
%    sigma=hyperparams(1,:);
%    alphas=hyperparams(2,:);
%    for k=1:numel(alphas);
%        for l=1:numel(alphas);
%        K=K+1/2/pi*alphas(k)*alphas(l)/(sigma(k)^2+sigma(l)^2)*exp(-1/2*D/(sigma(k)^2+sigma(l)^2));
%        end
%    end
%end

%keyboard

