function [K]=MexicanHatKernel_2(x,y,hyperparams);
%Mexican Hat Kernel function with parameters hyperparams(1)==width of
%positiv Gaussian, hyperparams(2)=width of negative Gaussian,
%hyperparams(3)=overall scaling


D=sum((x-y).^2,1);


%if numel(hyperparams)==2
%sigma=hyperparams;
%alphas=[1,-1];
hyperparams=[hyperparams(1),2*hyperparams(1),hyperparams(2)];


hyperparams(1)=hyperparams(1)*hyperparams(1);


hyperparams(2)=hyperparams(2)*hyperparams(2);

D1=exp(-1/4*D/hyperparams(1))/pi;
D2=exp(-1/4*D/hyperparams(2))/pi;
Dcross=exp(-1/2*D/(hyperparams(1)+hyperparams(2)))/pi;
K=0.25/hyperparams(1)*D1+0.25/hyperparams(2)*D2-1/(hyperparams(1)+hyperparams(2))*Dcross;
K=K*hyperparams(3);

%else
%    K=0;
%    sigma=hyperparams(1,:);
%    alphas=hyperparams(2,:);
%    for k=1:numel(alphas);
%        for l=1:numel(alphas);
%        K=K+1/2/pi*alphas(k)*alphas(l)/(sigma(k)^2+sigma(l)^2)*exp(-1/2*D/(sigma(k)^2+sigma(l)^2));
%        end
%    end
%end

%keyboard

