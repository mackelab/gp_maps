function [K,gradK]=MyDOG(varargin);
%at the moment, only works for stationary kernels, and requires as input a
%matrix of the pairwise distances between any two points, and derivative
%output does not work yet

if nargin==2
    D=varargin{1}.^2;
    hyperparams=varargin{2};
elseif nargin==3
    D=PWDistanceMult(varargin{1},varargin{2},'keepsquared');
    %D=sum((varargin{1}-varargin{2}).^2,2);
    hyperparams=varargin{3};
end



if numel(hyperparams)==2
    sigma=hyperparams;
    alphas=[1,-1];
    D1=1/4/pi/sigma(1)^2*exp(-1/2*D/2/sigma(1)^2);
    D2=1/4/pi/sigma(2)^2*exp(-1/2*D/2/sigma(2)^2);
    Dcross=1/pi/(sigma(1)^2+sigma(2)^2)*exp(-1/2*D/(sigma(1)^2+sigma(2)^2));
    K=D1+D2-Dcross;
else
    K=0;
    sigma=hyperparams(1,:);
    alphas=hyperparams(2,:);
    for k=1:numel(alphas);
        for l=1:numel(alphas);
        K=K+1/2/pi*alphas(k)*alphas(l)/(sigma(k)^2+sigma(l)^2)*exp(-1/2*D/(sigma(k)^2+sigma(l)^2));
        end
    end
end

%keyboard

