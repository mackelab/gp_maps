function B=My_Fourier_basis(ps1,ps2,nb,ordering)

if ~exist('nb','var'),
    nb=ps2;
end
if ~exist('ordering','var'),
    ordering='DC_center';
end

B=zeros(ps1*ps2,nb^2);
c=0;
for ky=0:(nb-1),
    for kx=1:nb,
        e=zeros(ps1*ps2,1);
        e(ky*ps1+kx)=1;
%        keyboard
        c=c+1;
        if ordering=='DC_center'
            keyboard
            B(:,c)=vec(ifft2(fftshift(reshape(e,ps1,ps2))));
        else
            B(:,c)=vec(ifft2((reshape(e,ps1,ps2))));
        end
    end
    fprintf(':')
end
B=imag(B)+real(B);
B=B/(norm(B(:,1)));
fprintf('\n')
