function D=OffDiag(M, nonan);

take=eye(size(M))==0;

D=M(take);

if nargin==2 & nonan==true
    D=D(~isnan(D));
end
