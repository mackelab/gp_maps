function P=Percentile(X,p);

%counts bottom up, i.e. p=100 is maximum.
m=round(size(X,1)*p/100);
m=max(1,m);
m=min(m,size(X,1));
X=sort(X,1);
P=X(m,:,:,:,:,:,:,:,:);
