function spec=PowerSpecImage(im, params);

%Ifft = abs(fftshift(fft2(I,w,h)));
%imshow(log(Ifft)/max(max(log(Ifft))));
%colormap(cool)

% function ftrans(image);
% performs freq transform of image, to obtain the fourier spectrum
% plots spectrum in 2D plot
% for each color separately (RGB) or once if Image is grey
% input: I(size,size,3) double precision;
% Stefanie Liebe, MPI Biological Cybernetics last modified 06/05


spec = fftshift(fft2(im));

