function [x,y,stims]=ReshapeData(r,orientations,selector);
%function [x,y,stims]=ReshapeData(r,orientations,selector);
%
%reshape data into the format the LearnGP wants

[pixx,pixy,numconds,numtrials]=size(r);
y=reshape(r,pixx*pixy,numconds*numtrials);
[xx,yy]=meshgrid(1:pixx,1:pixy);
x=[xx(:),yy(:)];

if nargin==3
    y=y(selector,:,:);
    x=x(selector,:);
end


if nargin==1
    orientations=linspace(0,2*pi,numconds+1);
    orientations=orientations(1:end-1);
end
stims=repmat(orientations(:),1,numtrials);
%keyboard
stims=stims(:);


