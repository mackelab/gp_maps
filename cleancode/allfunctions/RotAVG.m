function [rotmean, r, rotstd, n]=RotAVG(av2d, spacing)
%given a square image av2d, which has to have an odd number of elements,
%performs a rotational average rotman at radius points r, and also returns
%the standard deviation for each r, and the number of datapoints that was
%used to calculate it.
%if argument 'spacing' is given, it only evaluates the rotational average at
%bin sizes which are given by the vector spacing.

s1=(size(av2d,1)-1)/2;
s2=(size(av2d,2)-1)/2;
[x,y]=meshgrid(-s2:s2, -s1:s1);

rs=sqrt(x.^2+y.^2);

%keyboard
av2d=av2d(:);
rs=rs(:);
%keyboard
if nargin==1
    r=unique(rs);
    for k=1:numel(r);
        if mod(k,1000)==0
            PrintStar(k/1000);
        end
        %k/numel(r)
        myvals=av2d(rs==r(k));
        rotmean(k)=mean(myvals);
        n(k)=numel(myvals);
        rotstd(k)=std(myvals);
    end

elseif nargin==2
    for k=1:numel(spacing)-1
        r(k)=spacing(k);
%        keyboard
        myvals=av2d((rs>=spacing(k)-10^(-30)) & (rs<spacing(k+1)));
        rotmean(k)=mean(myvals);
        n(k)=numel(myvals);
        rotstd(k)=std(myvals);
        
    end

end









