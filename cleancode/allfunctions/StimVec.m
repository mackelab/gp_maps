function v=StimVec(theta,d)

if nargin==1
    d=5;
end

v=[cos(2*theta); sin(2*theta); cos(theta); sin(theta);sqrt(.5)];

v=v(1:d);



    
