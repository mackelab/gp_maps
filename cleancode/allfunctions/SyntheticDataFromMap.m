function [x,y,allthenoise]=SyntheticDataFromMap(map, orientations, numtrials, noiseops)
%function [x,y,allthenoise]=SyntheticDataFromMap(map, orientations, numtrials, noiseops)
%
%Simulate an imaging experiment by generating synthetic, noisy data from a
%supplied orientation preference map
%
%inputs:
%map: A complex valued matrix, the orientation preference map
%orientations: A vector of angles (in radians), the stimulus orientations
%for which responses should be simulated
%numtrials: The number of responses for each stimulus
%noiseops: A struct describing how the noise should be generated
%noiseops.model: 
%Use 'indep' for independent noise, the noiseopds.noisevar
%should be a matrix (or vector) of the point-wise noise variances for each
%pixel
%Use 'cov' if the whole noise-covariance is supplied (only works for small
%maps), noiseops.noisecov must then be the noise-covariance
%Use 'factoran' for a factor analysis model, in this case,
%noiseops.noisestd has to be a vector of standard-deviations for the
%independent noise component, and noiseops.G a matrix of the low-rank
%correlated noise components
%
%outputs:
%x is a 4-D array of the noisy simulated responses for each trial
%where x(:,:,a,b) is the response to the b-th repetition of the a-th
%stimulus
%
%y is a vector of the stimuli used to generated them (this output is a bit
%redundant, and not used anywhere else in the code, just left in for
%backwards compatiblity)
%
%allthenoise is an array of the noise components of the same size
%as x
%
%also see MakeToyData, CalcMap
%
%JHM 03/2010
%%%%%%%%%%%%



l=size(map,3);

switch noiseops.model
    case 'indep' 
        noisestd=sqrt(noiseops.noisevar);
    case 'conv'
      %generate noise by convolving independent noise with a point spread
      %function, not implemented
    case 'cov'
        cholo=col(noiseops.noisecov); %covariance matrix is supplied, of which we need the
        %cholesky-factor
    case 'factoran'
      %  keyboard
         noisestd=reshape(sqrt(noiseops.D),size(map,1),size(map,2));

end



for k=1:numel(orientations)
    if l==1
        actmap(:,:,k)=real(map)*cos(2*orientations(k))+imag(map)*sin(2*orientations(k));
    elseif l==2 %have four map components
        actmap(:,:,k)=real(map(:,:,1))*cos(2*orientations(k))+imag(map)*sin(2*orientations(k))...
            + real(map)*cos(orientations(k))+imag(map)*sin(orientations(k));
    elseif l==4 %have 8 map components
        actmap(:,:,k)=real(map(:,:,1))*cos(2*orientations(k))+imag(map)*sin(2*orientations(k))...
            + real(map)*cos(orientations(k))+imag(map)*sin(orientations(k))...
            + real(map)*cos(4*orientations(k))+imag(map)*sin(4*orientations(k));
    end


end

for k=1:numtrials
    for kk=1:numel(orientations)
        index=kk+(k-1)*numel(orientations);
        y(index)=orientations(kk);
        noise=randn(size(actmap(:,:,kk)));
        switch noiseops.model
            case {'indep','diag'}
                noise=noise.*noisestd;
            case 'conv'
            case 'cov'
                noise=cholo*noise;
            case 'factoran'
                comminput=randn(noiseops.q,1);
                noise=noise.*noisestd+reshape(noiseops.G*comminput,size(noise));
       
                
                
        end
        if nargout==3
            allthenoise(:,:,kk,k)=noise;
        end
        x(:,:,kk,k)=actmap(:,:,kk)+noise;
    end
end
