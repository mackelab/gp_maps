function [Bn,B]=make_Fourier_basis(psz,nb,ordering)

if ~exist('nb','var'),
    nb=psz;
end
if ~exist('ordering','var'),
    ordering='DC_center';
end

B=zeros(psz^2,nb^2);
c=0;
for ky=0:(nb-1),
    for kx=1:nb,
        e=zeros(psz^2,1);
        e(ky*psz+kx)=1;
        c=c+1;
        if ordering=='DC_center'
            B(:,c)=vec(ifft2(fftshift(reshape(e,psz,psz))));
        else
            B(:,c)=vec(ifft2((reshape(e,psz,psz))));
        end
    end
    fprintf(':')
end
B=imag(B)+real(B);
Bn=B/(norm(B(:,1)));
fprintf('\n')