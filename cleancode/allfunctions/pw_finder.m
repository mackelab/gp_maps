function [count,xx,yy] = pw_finder(z,rm,silent)
%%
%% e.g: [count,xx,yy]=pw_finder(z,0)
%%  or  simply [count,xx,yy]=pw_finder(z)
%%
%% INPUT:
%%
%%  z = re + i*im => angle map in a complex representation, area which is not part of the ROI should be set to z=-99.
%%  rm = number of pixels at the map's boundary to be dropped (default value: 0)
%%  boundary pinwheels are plotted in red and are DROPPED !
%%  
%% OUTPUT:
%%
%%  count = total number of pinweehls 
%%  xx  and yy = list of pw's coordinates
%%  
%% 
%%
%% (c) 2005 by Michael Schnabel, Max-Planck Institute for Dynamics & Self-Organization, mick[at]nld.ds.mpg.de
%% 

warning off MATLAB:divideByZero

if nargin<=2
  silent=true;
end
if ~silent
disp('(c) 2005 by Michael Schnabel, Max-Planck Institute for Dynamics & Self-Organization, mick[at]nld.ds.mpg.de')
end

x_angle=[];
aniso =[];
nx    =2;
ny    =2;
xx = [];
yy = [];

if nargin <2
rm      = 0 ;  %radius in pixel rad=10 is a good choice!
end

%warning off MATLAB:divideByZero %suppress warnings!
%no_write=1;

%%assumes -1< Re z, Im z <1 and ROI=0 where z =-99

PW = z*0;
[sy,sx]  =size(z);
roi=ones(size(z));
roi(find(z==-99))=0;


%% shrink the roi

tmp = tr_M2(roi);
tmp = tr_M2(tmp);    
tmp(find(tmp~=0))=1;  %% stipe of 8 pixels
roi_new = roi;
roi_new(find(tmp==1))=0;

roi_old = roi;
roi = roi_new;

ind = find(z~=-99);
z(ind)= z(ind)./mean(abs(z(ind))); %% normalization



doppeltestx=0;   %um doppeltzaehlung zu vermeiden!
doppeltesty=0;
doppeldet=0;  

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[grx,gry]=gradient(real(z));
[gix,giy]=gradient(imag(z));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

hf=figure(1); clf


subplot(nx,ny,1),[a,h]=contour(real(z),[0 0]);axis image,axis off, axis ij,drawnow;
chre=get(h,'Children');
lre=length(chre);


co=hsv(1024);
co(1024,:)=[1 1 1];
colormap(co)

subplot(nx,ny,2),[a,h]=contour(imag(z),[0 0]);axis image,axis off, axis ij,drawnow;
chim=get(h,'Children');
lim=length(chim);

count=0;

subplot(nx,ny,4),hold on
contour(real(z),[0 0],'k'); 
contour(imag(z),[0 0],'k'),axis image,axis off, axis ij,hold on; %must have same colour!!!
                           %(dont ask why!!)


subplot(nx,ny,3)

%load ./colormap_16_cnt.mat  %% -> col;
an = mod(angle(z),2*pi);
%rgb= to_rgb(an,col,0,2*pi,roi_old);

imagesc(an);
axis image,axis off, axis ij,hold on
contour(real(z),[0 0],'k'); 
contour(imag(z),[0 0],'w');
%drawnow

 % figure(3),clf
%% calculate distances & rectangles in advance!
 
 
 for s= 1:lim
     
     
     xim =get(chim(s),'XData')';
     yim =get(chim(s),'YData')';
     l=length(xim)-1;
   
     Xim(s,1:l) =xim(1:l);
     Yim(s,1:l) =yim(1:l);  %'NaN abschneiden)
     Nim(s)     = l;
     
     dist2=max((xim(1:l-1)-xim(2:l)).^2+(yim(1:l-1)-yim(2:l)).^2);
     Dist2(s)=dist2;
     
     im_4(s,1:4)= [min(xim) max(xim) min(yim) max(yim)];
     
     %figure(3)
     %rectangle('position',[min(xim),min(yim),max(xim)-min(xim),max(yim)-min(yim)],'edgecolor','r')
     %hold on, drawnow
     
 end
 
 for s= 1:lre
     
     xre =get(chre(s),'XData')';
     yre =get(chre(s),'YData')';
     l=length(xre)-1;
   
     Xre(s,1:l) =xre(1:l);
     Yre(s,1:l) =yre(1:l);  %'NaN abschneiden)
     Nre(s)     = l;
     
     dist1=max((xre(1:l-1)-xre(2:l)).^2+(yre(1:l-1)-yre(2:l)).^2);
     Dist1(s)=dist1;
     
     re_4(s,1:4)= [min(xre) max(xre) min(yre) max(yre)];
     
     %figure(3)
     %rectangle('position',[min(xre),min(yre),max(xre)-min(xre),max(yre)-min(yre)],'edgecolor','k')
     %hold on, drawnow
     
 end
 
 
 %% find out which contour speaks to which
 M = zeros(lre,lim);
 
 for t =1:lre
     
     min_x_re = re_4(t,1);
     max_x_re = re_4(t,2);
     min_y_re = re_4(t,3);
     max_y_re = re_4(t,4);
     
     
     for s=1:lim
         
         min_x_im = im_4(s,1);
         max_x_im = im_4(s,2);
         min_y_im = im_4(s,3);
         max_y_im = im_4(s,4);
     
          

         if ( (((min_x_re <= min_x_im) & (min_x_im <= max_x_re)) || ...
               ((min_x_re <= max_x_im) & (max_x_im <= max_x_re)) || ...
               ((min_x_im <= min_x_re) & (min_x_re <= max_x_im)) || ...
               ((min_x_im <= max_x_re) & (max_x_re <= max_x_im))) & ...
              (((min_y_re <= min_y_im) & (min_y_im <= max_y_re)) || ...
               ((min_y_re <= max_y_im) & (max_y_im <= max_y_re)) || ...
               ((min_y_im <= min_y_re) & (min_y_re <= max_y_im)) || ...
               ((min_y_im <= max_y_re) & (max_y_re <= max_y_im))) )
              
             M(t,s)=1;
                 
      
         end
        
     end
 end
 %figure(4), imagesc(M), axis image,colorbar
 
figure(1)  
for t=1:lre
  
  lr=Nre(t);
  xre = Xre(t,:);
  yre = Yre(t,:);  
  xre = xre(1:lr);
  yre = yre(1:lr);
  
  dist1=Dist1(t);
   
  mm  = M(t,:);
  s2t = find(mm==1); 
  
  for ss= 1:length(s2t);
      
     s = s2t(ss);
     
     li   = Nim(s);
     
     xim = Xim(s,:);
     yim = Yim(s,:);
     xim = xim(1:li);
     yim = yim(1:li);
   
     dist2=Dist2(s);
  
     de = 2*max(dist1,dist2);
          
     %tx         = xim(1:li-1);
     %ty         = yim(1:li-1);
     dist = zeros(1,li);
         for p=1:lr-1
	               
          dist = (xre(p)-xim).^2 + (yre(p)-yim).^2;
          %%figure(2),plot(xre(p),yre(p),'ob'),figure(1)
          
       	  index= find ( dist <= de);
                  
	     
           for q=1:length(index);
	          
             if index(q)<=li-1;
              
              cx=xim(index(q));
	          cy=yim(index(q));
	          %%figure(2),plot(cx,cy,'dm'),figure(1)
              
	            
                 dx=xim(index(q)+1);
	             dy=yim(index(q)+1);
	             ax=xre(p);
	             ay=yre(p);
	             bx=xre(p+1);
	             by=yre(p+1);
	             %%m=((cx-ax)*(by-ay)-(cy-ay)*(bx-ax))/((cx-dx)*(by-ay)-(cy-dy)*(bx-ax));
	             %%n=((cx-ax)*(cy-dy)-(cy-ay)*(cx-dx))/((bx-ax)*(cy-dy)-(by-ay)*(cx-dx)); 
	   
	             
                 %%if ((0 <= m) & (m <= 1) & (0<=n) &(n<=1)),count=count+1; 
                 [answer,m,n] =  dotheycross(ax,ay,bx,by,cx,cy,dx,dy);
	             if (answer),count=count+1;
                 
	                pinx=cx+m*(dx-cx);
	                piny=cy+m*(dy-cy);
                    %%disp([count,pinx,piny]);
                    
                    x = pinx;
                    y = piny;
% 	                x=round(cx+m*(dx-cx));      %% changed 17/01/07 -> take exact values
% 		            y=round(cy+m*(dy-cy));
% 		
		            %Die plot routine transponiert (warum!?) die Matrix, deshalb:
		
	            	qx=x;
	            	qy=y;
		
             		x=qy;
            		y=qx;
		
		
		 
	             	a=est(grx,x,y); %grad_x Re(z)
		            b=est(gry,x,y); %grad_y Re(z)
	            	c=est(gix,x,y); %grad_x Im(z)
		            d=est(giy,x,y); %grad_y Im(z)
	
		
	            	det=a*d-b*c;  %Pw-Drehsinn
		
	            	%Koordinaten wieder zuruecktransponieren....
		
		            x=qx;
		            y=qy;
		
                    %checking whether pw surrounding does touch the mask
                    
                    touch_mask=0;
        
                    for  phi = (0:.01:2*pi-.01);
                      
                        %xr=round(x+rm*cos(phi));
                        %yr=round(y+rm*sin(phi));
                        xr=(x+rm*cos(phi));
                        yr=(y+rm*sin(phi));
                        
                      
                        if xr>1 & xr <sx & yr>1 & yr <sy 
                
                           if est(roi,yr,xr)==0 touch_mask=1;, break 
                           end;
                           
                           else touch_mask=1; break %% near matrix edge!
                           
                        end
                     end
        
		
		            if   (qx==doppeltestx) & (qy==doppeltesty) & ...
		                 (sign(det)==doppeldet)
		                 
                     
                        'doppelzaehlung vermieden';
		                 count = count-1;
                         
                         else
		  
                         subplot(nx,ny,4)
                         hold on
          
           
                         if x<sx & y <sy & x>0 & y>0

               
                              if (est(roi,y,x)==1) & (touch_mask==0) plot(x,y,'bo'),axis ij; 

                                  %[an th ph si]=pw_parameter([a,b,c,d]');
                                  %aniso=[aniso an];
                                  xangle=acos((a*c+b*d)/(sqrt(a^2+b^2)*sqrt(c^2+d^2)))/pi*180;
                                  x_angle=[x_angle xangle];
                                
                                  xx = [xx pinx];
                                  yy = [yy piny];
                                  % if ~no_write
                                  % 		                        
                                  %                                      fprintf(fid,'%g %g %g %g %g %g %g %g %g %g %g\n',[count,pinx,...
                                  % 		                              piny,a,b,c,d,an,th,ph,si]);
                                  %                              
                                  %                                      %fprintf(fid,'%g %g %g \n',[count,pinx,...
                                  % 		                              %   piny]);
                                  %              
                                  %                                   end
                                  %                                   
                                   else  plot(x,y,'r*'),axis ij;
                                   count = count-1;  
                              end
                              
                              else count = count-1;
      
	                     end  
	                     
		                 doppeltestx=qx;
	                   	 doppeltesty=qy;
		                 doppeldet=sign(det);
	
		
                         
                         
                 end
                 
             end   
             
           end    
         end  
          
          
      end
	 
      
  end
  
  
 end
 
 

if silent
    close(hf)
end  



function [answer,m,n] = dotheycross(ax,ay,bx,by,cx,cy,dx,dy)

 m=((cx-ax)*(by-ay)-(cy-ay)*(bx-ax))/((cx-dx)*(by-ay)-(cy-dy)*(bx-ax));
	     n=((cx-ax)*(cy-dy)-(cy-ay)*(cx-dx))/((bx-ax)*(cy-dy)-(by-ay)*(cx-dx)); 
	   
 if ((0 <= m) & (m <= 1) & (0<=n) &(n<=1)) answer = true;
    else answer = false;
 end


 
 function z_est = est(z,x,y);
%% linear interpolation to guess z(10.24,56.86)
xs = floor(x);
xm =  ceil(x);
ys = floor(y);
ym =  ceil(y);

%% determine which quadrant!

d(1) = (x-xs)^2+(y-ys)^2;
d(2) = (x-xs)^2+(y-ym)^2;
d(3) = (x-xm)^2+(y-ym)^2;
d(4) = (x-xm)^2+(y-ys)^2;

ind = find(d==min(d));
ind=ind(1);

if ind ==1 
    d1  = x-xs;
    d2  = y-ys;
    z0  = z(xs,ys);
    z1  = (z(xm,ys)-z0);
    z2  = (z(xs,ym)-z0);
end

if ind ==2
    d1  = x-xs;
    d2  = y-ym;
    z0  = z(xs,ym);
    z1  = (z(xm,ym)-z0);
    z2  = (z(xs,ys)-z0);
end

if ind ==3
    d1  = x-xm;
    d2  = y-ym;
    z0  = z(xm,ym);
    z1  = (z(xs,ym)-z0);
    z2  = (z(xm,ys)-z0);
end

if ind ==4
    d1  = x-xm;
    d2  = y-ys;
    z0  = z(xm,ys);
    z1  = (z(xm,ym)-z0);
    z2  = (z(xs,ys)-z0);
end

z_est  = z0+ z1*d1 + z2*d2;

function tmp = tr_M2(r);
%% calcluates  r_xx.^2+ r_yy.^2+2 r_xy.^2
 [r_x,r_y]=gradient(r);
 
 [r_xx,r_xy]=gradient(r_x);
 [r_yx,r_yy]=gradient(r_y);
 
 tmp = r_xx.^2+ r_yy.^2 + 2*r_xy.^2;
 

