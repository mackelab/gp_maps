function A=reshapemap(map);

l=size(map,1);
x=sqrt(l);
A=reshape(map(:,1)+i*map(:,2),x,x);