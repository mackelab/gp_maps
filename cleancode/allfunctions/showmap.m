function [handles,theta,r]=showmap(map, modus, minx, maxx, mask, smoothcontours,zerocrossings)
%function [h,ha,theta,r]=showmap(map, modus, minx, maxx, mask)
%
%show angular part of complex-valued (or multidimensional) orientation
%preference map, with several modi for display available:
%
%inputs:
%
%map: The orientation preference map, either complex valued, or
%3dimensional, in which case map(:,:,1) has to be the real part, and
%map(:,:,2) the complex part. 
%
%modus 'anglesonly': Display angle only, throw out information about
%amplitudes
%modus 'shadetogray': Set un-selective pixels to gray (i.e. pixels for
%which the amplitude is less than minx), and interpolate saturation to
%fully coloured for pixels for whith the amplitude is greater than maxx
%modus 'shadetoblack': Set un-selective pixels to black, otherwise as above
%modus 'shadetowhite;: Set un-selectgive pixels to white, otherwise as
%above
%modus 'cliptowhite': Set un-selective pixels to white, otherwise use full
%saturation
%
%minx: lower threshold for amplitudes. Lower amplitudes are displayed as
%black (or white or gray, depending on the color mode). If a negative value is supplied,
% percentiles are used rather than absoluted values (i.e. -10 means that all
% values blower than the 10th percentile are displayed in white/black).
% Default is -10.
%
%maxx: upper threshold for amplitudes. Larger amplitudes are displayed as
%fully saturated colors. If a negative value is supplied, percentiles are
%used rather than absoluted values (i.e. -90 means that all values bigger
%than the 90th percentile are displayed in full saturation). Default is
%-90.
%
%mask: Region of interest, a matrix of the same size as map, with ones for
%interesting pixels, and zeros for everything else. Boring pixels are
%displayed as white. Default is that all pixels are interesting.
%
%smoothcontours: If supplied and greater than 0, the amplitudes of the map
%(but not the angles!) are smoothed with a Gaussian window of width
%smoothcontours
%
%zerocrossings: If supplied and not equal to 0, the zero-crossings of the
%real and imaginary part are colored in, using the color specified in
%'zerocrossings'
%
%outputs:
%
%handles: graphics handles, for all sorts of stuff
%
%theta: angle map
%
%r: map of amplitudes
%
%JHM 03/2010
%
%

if size(map,3)<=5 && isreal(map)
    map=map(:,:,1)+i*map(:,:,2);
end

if nargin<=1
    modus='anglesonly';
end

if nargin<=4 || isempty(mask)
    mask=logical(ones(size(map)));
end

theta=angle(map);
r=abs(map);

if nargin<=5 || isempty(smoothcontours)
    smoothcontours=0;
end

if smoothcontours>0
   % keyboard
r=conv2fft(r,GaussFilter(20,smoothcontours),'same');
end

if nargin<=2 || isempty(minx)
    minx=Percentile(r(mask),10);
elseif minx<0
    minx=Percentile(r(mask),-minx);
end

if nargin<=3 || isempty(maxx);
    maxx=Percentile(r(mask),90);
elseif maxx<0
    maxx=Percentile(r(mask),-maxx);
end





if nargin<=5 || isempty(zerocrossings)
    zerocrossings=0;
end






r=min(r,maxx);
r=max(r,minx);

theta=(theta+pi)/2/pi;
theta=round(128*theta);
theta=min(128,theta);
theta=max(1,theta);

r=(r-minx)/(maxx-minx);

colors=zeros(size(theta,1), size(theta,2),3);

mcolormap=hsv(128);


for k=1:3
    loccolors=colors(:,:,k);
    loccolors=reshape(mcolormap(theta,k),size(theta));
    switch modus
        case 'anglesonly'
        case 'shadetogray'
        loccolors=(loccolors.*r)/2+0.5;
        case 'shadetoblack'
        loccolors=(loccolors.*r);    
        case 'shadetowhite'
        loccolors=loccolors+(1-loccolors).*(1-r);    
        case 'cliptowhite'
        threshit=double(r>minx);
        loccolors=loccolors.*threshit+(1-threshit);
        otherwise 
            error('Color mode not recognized, see help for recognized modes')
    end
    loccolors(~mask)=1;
    colors(:,:,k)=loccolors;
   
end




handles.image=imagesc(colors);
handles.axes=gca;
colormap(mcolormap)
axis equal, axis tight


if smoothcontours>0
map=conv2fft(map,GaussFilter(20,smoothcontours),'same');
end


if zerocrossings~=0
    hold on
   [handles.realcontourcoords,handles.realcontour]=contour(real(map), [0,0],zerocrossings);
   [handles.imagcontourcoords,handles.imagcontour]=contour(imag(map), [0,0],zerocrossings);
   hold off
end






