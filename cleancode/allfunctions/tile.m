function A=tile(a,b)
%use repmat in order to make a the same size as b. At the moment, only
%works for a and b being two-dimensional.

asize=size(a);
bsize=size(b);

if numel(bsize)> numel(asize)
    asize=[asize,ones(1,numel(bsize)-numel(asize))];
end

%keyboard

multiplier=bsize./asize;

A=repmat(a, multiplier);
